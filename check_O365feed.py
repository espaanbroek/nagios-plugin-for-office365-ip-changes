#!/usr/bin/env python

# Import modules
import feedparser
import argparse
import datetime
from sys import exit

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--feed", required=True,  help="adress of the RSS-feed")
ap.add_argument("-b", "--behind", required=True, help="look days <behind> current day")
ap.add_argument("-p", "--product", required=True, help="product name, eg. Skype for Business Online")

args = vars(ap.parse_args())

# Variabelen met initiële waarden
product = args["product"]
feed = args["feed"]
daysbehind = int(args["behind"])
present = datetime.datetime.now()

# Function to return message and exit code to Nagios
def stattonagios(msg, status):
  if status == 0:
    statmsg = "OK: " + msg
  elif status == 1:
    statmsg = "WARNING: " + msg
  elif status == 2:
    statmsg = "CRITICAL: " + msg
  elif status == 3:
    statmsg = "UNKNOWN: " + msg

  print(statmsg)
  exit(status)

# Parse the feed and return the last days of entries set with the variable 'daysbehind'. Return UNKNOWN to Nagios if error.
d = feedparser.parse(feed)
try:
  if len(d.version)>0:
    for i in d.entries:
      if d.entries:
        title = i.title
        pubdate = i.published
        pubdatefmt = datetime.datetime.strptime(pubdate, '%a, %d %b %Y %H:%M:%S %Z')
        if title.startswith(product):
          datedelta = present - pubdatefmt
          if datedelta.days < daysbehind:
            status = 1
            msg = f"Er zijn IP wijzigingen voor {product}. Bron: {feed}."
            stattonagios(msg, status)
      else:
        msg = f"Kan RSSFeed {feed} niet laden."
        status = 2
        stattonagios(msg, status)
  else:
     msg = f"RSSFeed {feed} kan niet verwerkt worden."
     status = 3
     stattonagios(msg, status)
except (AttributeError,KeyError):
  msg = f"{feed} is geen RSSfeed."
  status = 3
  stattonagios(msg, status)

# If the script reaches this point there are no changes
msg = f"Geen IP wijzigingen voor {product}."
status = 0
stattonagios(msg, status)